package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Stack implements  StackOperations {

    private List<String> stack = new ArrayList<>();

    @Override
    public List<String> get() {
        return stack;
    }

    @Override
    public Optional<String> pop() {
        int size = stack.size() - 1;
        if(size <= 0){
            return Optional.empty();
        }
        return Optional.ofNullable(stack.remove(size));
    }

    @Override
    public void push(String item) {
        stack.add(item);
    }
}
